import re
import random
import torch.utils.data
import Levenshtein as Lev
import spacy
import collections

_sos = '<s>'
_eos = '</s>'
_pad = '<pad>'

_punc = '!\'"(),-.:;? '

_eng_characters = 'abcdefghijklmnopqrstuvwxyz'

_arpabet_characters = [
    'AA', 'AE', 'AH',
    'AO', 'AW', 'AY',
    'B', 'CH', 'D', 'DH', 'EH', 'ER', 'EY',
    'F', 'G', 'HH', 'IH', 'IY',
    'JH', 'K', 'L', 'M', 'N', 'NG', 'OW', 'OY',
    'P', 'R', 'S', 'SH', 'T', 'TH', 'UH', 'UW',
    'V', 'W', 'Y', 'Z', 'ZH'
]
_arpabet_characters = ['@' + s for s in _arpabet_characters]
eng_symbols = [_pad, _sos, _eos] + list(_punc) + list(_eng_characters)
arpabet_symbols = [_pad, _sos, _eos] + list(_punc) + _arpabet_characters

pos_symbols = [
    _pad, _sos, _eos,
    'ADJ', 'ADP', 'ADV', 'AUX',
    'CONJ', 'CCONJ',
    'DET',
    'INTJ',
    'NOUN', 'NUM',
    'PART', 'PRON', 'PROPN', 'PUNCT',
    'SCONJ', 'SYM',
    'VERB',
    'X',
    'SPACE'
]

eng_to_id = {s: i for i, s in enumerate(eng_symbols)}
id_to_eng = {i: s for i, s in enumerate(eng_symbols)}

arpabet_to_id = {s: i for i, s in enumerate(arpabet_symbols)}
id_to_arpabet = {i: s for i, s in enumerate(arpabet_symbols)}

pos_to_id = {s: i for i, s in enumerate(pos_symbols)}
id_to_pos = {i: s for i, s in enumerate(pos_symbols)}

_curly_re = re.compile(r'(.*?)\{(.+?)\}(.*)')
_input_re = re.compile(r'[^a-zA-Z!\'"(),-.:;? ]')
_eng_re = re.compile(r'\b\S+\b')
_whitespace_re = re.compile(r'\s+')

_nlp = spacy.load("en_core_web_sm")

Replacement = collections.namedtuple("replacement", ['start', 'end', 'text'])


def normalize_text(text):
    text = re.sub(_whitespace_re, ' ', text)
    text = re.sub(_input_re, '', text)
    return text.strip()


def english_cleaners(text):
    sequence = [eng_to_id[c] for c in text.lower()]
    sequence.append(eng_to_id[_eos])
    return sequence


def arpabet_cleaners(text):
    sequence = []
    while len(text):
        m = _curly_re.match(text)
        if not m:
            sequence.extend([arpabet_to_id[c] for c in text.lower() if c in arpabet_to_id])
            break
        sequence.extend([arpabet_to_id[c] for c in m.group(1).lower() if c in arpabet_to_id])
        sequence.extend([arpabet_to_id['@' + s] for s in m.group(2).upper().split() if '@' + s in arpabet_to_id])
        text = m.group(3)
    sequence.append(arpabet_to_id[_eos])
    return sequence


def tag_pos(text):
    doc = _nlp(text)

    pos_list = []
    previous_end = 0
    for token in doc:
        if previous_end != token.idx:
            pos_list.extend([pos_to_id['SPACE']] * (token.idx - previous_end))
        pos_list.extend([pos_to_id[token.pos_]] * len(token.text))

        previous_end = token.idx + len(token.text)
    pos_list.append(pos_to_id[_eos])
    return pos_list


def find_replacements(text, g2p_dict):
    replacements = []
    for match in _eng_re.finditer(text):
        if match.group() in g2p_dict:
            replacements.append(
                Replacement(
                    start=match.start(),
                    end=match.end(),
                    text=g2p_dict[match.group()]
                )
            )
    return replacements


def _sequence_to_text(sequence, id_to_char):
    target_text = []
    for c in sequence:
        c = c.item()
        if c == 2:
            break
        elif c < 2:
            continue
        s = id_to_char[c]
        if s in _arpabet_characters:
            s = '{%s}' % s
        target_text.append(s)
    target_text = ''.join(target_text)
    return target_text.replace('}{', ' ').replace('@', '')


def sequence_to_eng(sequence):
    return _sequence_to_text(sequence, id_to_eng)


def sequence_to_arpabet(sequence, eng_sequence=None, alignment=None, replacements=None):
    if replacements is not None and 0 < len(replacements):
        result = []
        position = 0
        alignment_arpabet_indices = torch.argmax(alignment, dim=0)
        alignment_eng_indices = torch.argmax(alignment, dim=1)
        for replacement in replacements:
            if eng_sequence[replacement.start].item() <= 14 or eng_sequence[replacement.end - 1].item() <= 14:
                continue
            sequence_start = alignment_arpabet_indices[replacement.start].item()
            sequence_end = alignment_arpabet_indices[replacement.end - 1].item()

            for arpabet_idx, eng_idx in enumerate(alignment_eng_indices):
                if sequence[arpabet_idx].item() <= 14:
                    continue
                eng_idx = eng_idx.item()
                if eng_idx == replacement.start and arpabet_idx < sequence_start:
                    sequence_start = arpabet_idx
                if eng_idx == replacement.end - 1 and sequence_end < arpabet_idx:
                    sequence_end = arpabet_idx
            while sequence[sequence_end].item() <= 14 and sequence_start < sequence_end:
                sequence_end -= 1
            if position <= sequence_start:
                if position < sequence_start:
                    result.append(_sequence_to_text(sequence[position:sequence_start], id_to_arpabet))
                result.append(replacement.text)
                position = sequence_end + 1
        result.append(_sequence_to_text(sequence[position:], id_to_arpabet))
        return "".join(result)
    else:
        return _sequence_to_text(sequence, id_to_arpabet)


def calculate_ler(prediction, target):
    """
    Computes the Character Error Rate, defined as the edit distance.
    Arguments:
        prediction (string): space-separated sentence
        target (string): space-separated sentence
    """
    prediction = arpabet_cleaners(prediction)
    target = arpabet_cleaners(target)
    prediction = ''.join([chr(idx) for idx in prediction if id_to_arpabet[idx] in _arpabet_characters])
    target = ''.join([chr(idx) for idx in target if id_to_arpabet[idx] in _arpabet_characters])
    return Lev.distance(prediction, target), len(target)


class TransliteratorDataset(torch.utils.data.Dataset):
    def __init__(self, txt_file, hparams, training):
        self.training = training
        self.data_list = []
        with open(txt_file, 'r', encoding='utf-8') as rf:
            for line in rf.readlines():
                data = line.replace('\n', '').split('\t')
                eng = data[0]
                arpabet = data[1]
                self.data_list.append((eng, arpabet))
        random.seed(hparams.seed)
        random.shuffle(self.data_list)

        self.eng_pattern = re.compile(r"[a-z][a-z']*[a-z]|[a-z]")
        self.arpabet_pattern = re.compile(r"{[^}]+}")

    def __getitem__(self, index):
        eng, arpabet = self.data_list[index]

        eng = normalize_text(eng)

        pos_list = tag_pos(eng)
        pos_list = torch.IntTensor(pos_list)
        # if self.training:
        #     eng, pos_list = self._augment(eng, arpabet, pos_list)

        eng = english_cleaners(eng)
        eng = torch.IntTensor(eng)

        arpabet = arpabet_cleaners(arpabet)
        arpabet = torch.IntTensor(arpabet)

        return eng, pos_list, arpabet

    def __len__(self):
        return len(self.data_list)

    def _augment(self, eng, arpabet):
        if random.random() < 0.5:
            eng_items = [m for m in self.eng_pattern.finditer(eng)]
            arpabet_items = [m for m in self.arpabet_pattern.finditer(arpabet)]

            assert len(eng_items) == len(arpabet_items)

            previous_end = 0
            output = ""
            for eng_m, arpabet_m in zip(eng_items, arpabet_items):
                output += eng[previous_end:eng_m.start()]
                if random.random() < 0.5:
                    output += arpabet_m.group()
                else:
                    output += eng_m.group()
                previous_end = eng_m.end()
            output += eng[previous_end:]
            return output
        else:
            return eng


class TextCollate:
    def __call__(self, batch):
        input_lengths, ids_sorted_decreasing = torch.sort(
            torch.LongTensor([len(x[0]) for x in batch]),
            dim=0, descending=True)
        max_input_len = input_lengths[0]

        input_padded = torch.LongTensor(len(batch), max_input_len)
        input_padded.zero_()

        pos_padded = torch.LongTensor(len(batch), max_input_len)
        pos_padded.zero_()
        for i in range(len(ids_sorted_decreasing)):
            text = batch[ids_sorted_decreasing[i]][0]
            pos = batch[ids_sorted_decreasing[i]][1]
            input_padded[i, :text.size(0)] = text
            pos_padded[i, :pos.size(0)] = pos

        max_output_len = max([x[1].size(0) for x in batch])

        output_padded = torch.LongTensor(len(batch), max_output_len)
        output_padded.zero_()
        output_lengths = torch.LongTensor(len(batch))
        for i in range(len(ids_sorted_decreasing)):
            text = batch[ids_sorted_decreasing[i]][2]
            output_padded[i, :text.size(0)] = text
            output_lengths[i] = text.size(0)

        return input_padded, pos_padded, input_lengths, output_padded, output_lengths
