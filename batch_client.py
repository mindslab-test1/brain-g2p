import argparse

from functools import partial
from tqdm import tqdm
from contextlib import closing
from multiprocessing import Pool

from client import G2PClient
from data import calculate_ler


def parallel_run(fn, items, desc="", n_cpu=4):
    results = []

    if n_cpu > 1:
        with closing(Pool(n_cpu)) as pool:
            for out in tqdm(pool.imap_unordered(
                    fn, items), total=len(items), desc=desc):
                if out is not None:
                    results.append(out)
    else:
        for item in tqdm(items, total=len(items), desc=desc):
            out = fn(item)
            if out is not None:
                results.append(out)

    return results


def test(data, g2p):
    input = data[0]
    target = data[1]
    g2p_client = G2PClient(g2p)
    pred = g2p_client.transliterate([input])
    pred = pred.sentences[0]

    data.append(pred)

    letter_err_cnt_i, letter_tot_cnt_i = calculate_ler(pred, target)
    data.append((letter_err_cnt_i, letter_tot_cnt_i))

    return data


def test_batch(data_list, n_cpu, **kargv):
    test_fn = partial(test, **kargv)

    results = parallel_run(test_fn, data_list,
                           desc="test", n_cpu=n_cpu)
    return results


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--g2p',
                        nargs='?',
                        help='g2p grpc server=ip:port',
                        type=str,
                        default='127.0.0.1:19001')
    parser.add_argument('-i', '--input',
                        nargs='?',
                        help='input file',
                        type=str,
                        required=True)
    parser.add_argument('-e', '--encoding',
                        type=str,
                        default='utf-8',
                        help='txt file encoding')
    parser.add_argument('-o', '--output',
                        nargs='?',
                        help='output file',
                        type=str,
                        default='output.txt')
    parser.add_argument('-n', '--n_cpu',
                        type=int,
                        default=1,
                        help='number of processes')

    args = parser.parse_args()

    data_list = []
    with open(args.input, 'r', encoding=args.encoding) as rf:
        for line in rf.readlines():
            data = line.split('\t')
            input = data[0]
            target = data[1].replace('\n', '')
            data = [input, target]
            data_list.append(data)

    results = test_batch(data_list, args.n_cpu,
                         g2p=args.g2p)

    if len(results) > 0:
        err_tot_cnt = 0
        target_tot_cnt = 0
        with open(args.output, 'w', encoding=args.encoding) as wf:
            for data in results:
                err_cnt, target_cnt = data[-1]
                err_tot_cnt += err_cnt
                target_tot_cnt += target_cnt
                data = [str(d) for d in data]
                wf.write("\t".join(data))
                wf.write("\n")
            er = err_tot_cnt / target_tot_cnt
            print('error rate: {}%'.format(er * 100))
