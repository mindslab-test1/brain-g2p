#!/usr/bin/env python3.5
# -*- coding: UTF-8 -*-

import logging
import time
import torch
import grpc
import argparse
import signal

from concurrent import futures
from grpc_health.v1 import health
from grpc_health.v1 import health_pb2_grpc

from data import english_cleaners, sequence_to_arpabet, tag_pos, find_replacements, normalize_text
from train import load_model
from g2p_pb2_grpc import add_G2PServicer_to_server, G2PServicer
from g2p_pb2 import Phoneme

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


def load_g2p_dict(file_path):
    g2p_dict = {}
    with open(file_path, 'r', encoding='utf-8') as rf:
        for line in rf.readlines():
            data = line.split('=')
            eng = data[0]
            arpabet = data[1].replace('\n', '')
            g2p_dict[eng] = arpabet
    return g2p_dict


class G2PServicerImpl(G2PServicer):
    def __init__(self, checkpoint_path, dict_path, device, max_decoder_steps):
        super().__init__()
        self.g2p_dict = load_g2p_dict(dict_path)
        torch.cuda.set_device(device)
        self.device = device

        checkpoint = torch.load(checkpoint_path, map_location='cpu')
        hparams = checkpoint['hparams']
        hparams.max_decoder_steps = max_decoder_steps

        state_dict = checkpoint['state_dict']

        self.model = load_model(hparams)
        self.model.load_state_dict(state_dict)
        self.model.eval()

        del hparams
        del state_dict
        del checkpoint
        torch.cuda.empty_cache()

    def Transliterate(self, request, context):
        try:
            torch.cuda.set_device(self.device)
            with torch.no_grad():
                english = request.sentences
                logging.debug('request:%s', english)
                english, output_ids, replacements = self._preprocess(english)
                logging.debug('output_ids:%s, replacements:%s', output_ids, replacements)
                arpabet, alignments = self.model.inference(*english)
                arpabet = [
                    sequence_to_arpabet(
                        arpabet[idx.item()],
                        english[0][idx.item()],
                        alignments[idx.item()],
                        replacements[idx.item()]
                    ) for idx in output_ids
                ]
                logging.debug('response:%s', arpabet)
                return Phoneme(sentences=arpabet)
        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

    def _preprocess(self, sentence_list):
        input_list = []
        for sentence in sentence_list:
            sentence = normalize_text(sentence)
            if sentence[-1] not in "?!.":
                sentence = sentence + '.'
            input_list.append(sentence)
        pos_list = [torch.cuda.LongTensor(tag_pos(text)) for text in input_list]
        text_list = [torch.cuda.LongTensor(english_cleaners(text)) for text in input_list]
        input_lengths, ids_sorted_decreasing = torch.sort(
            torch.cuda.LongTensor([len(text) for text in text_list]),
            dim=0, descending=True)
        max_input_len = input_lengths[0]

        input_padded = torch.cuda.LongTensor(len(text_list), max_input_len)
        input_padded.zero_()
        pos_padded = torch.cuda.LongTensor(len(text_list), max_input_len)
        pos_padded.zero_()
        replacements = []
        for i in range(ids_sorted_decreasing.size(0)):
            text = text_list[ids_sorted_decreasing[i]]
            input_padded[i, :text.size(0)] = text
            pos = pos_list[ids_sorted_decreasing[i]]
            pos_padded[i, :pos.size(0)] = pos

            replacement = find_replacements(
                input_list[ids_sorted_decreasing[i]],
                self.g2p_dict
            )
            replacements.append(replacement)

        output_ids = torch.sort(ids_sorted_decreasing)[1]
        return (input_padded, pos_padded, input_lengths), output_ids, replacements


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='g2p runner executor')
    parser.add_argument('-m', '--model',
                        nargs='?',
                        dest='model',
                        required=True,
                        help='Model Path.',
                        type=str)
    parser.add_argument('--dict',
                        nargs='?',
                        dest='dict',
                        help='G2P Dictionary Path.',
                        type=str,
                        default='g2p.dict')
    parser.add_argument('-l', '--log_level',
                        nargs='?',
                        dest='log_level',
                        help='logger level',
                        type=str,
                        default='INFO')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        help='grpc port',
                        type=int,
                        default=19001)
    parser.add_argument('-d', '--device',
                        nargs='?',
                        dest='device',
                        help='gpu device',
                        type=int,
                        default=0)
    parser.add_argument('-s', '--max_decoder_steps',
                        nargs='?',
                        dest='max_decoder_steps',
                        help='arpabet length limit',
                        type=int,
                        default=1000)

    args = parser.parse_args()

    g2p_servicer = G2PServicerImpl(args.model, args.dict, args.device, args.max_decoder_steps)

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1),)
    add_G2PServicer_to_server(g2p_servicer, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    health_servicer = health.HealthServicer()
    health_pb2_grpc.add_HealthServicer_to_server(health_servicer, server)
    server.start()

    logging.basicConfig(
        level=getattr(logging, args.log_level),
        format='[%(levelname)s|%(filename)s:%(lineno)s][%(asctime)s] >>> %(message)s'
    )

    def exit_gracefully(signum, frame):
        health_servicer.enter_graceful_shutdown()
        server.stop(60)
    signal.signal(signal.SIGINT, exit_gracefully)
    signal.signal(signal.SIGTERM, exit_gracefully)
    logging.info('g2p starting at 0.0.0.0:%d', args.port)

    server.wait_for_termination()
