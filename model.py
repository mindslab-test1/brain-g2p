import logging
import random
import torch
import torch.nn as nn
import torch.nn.functional as F

from torch.autograd import Variable


def get_mask_from_lengths(lengths):
    max_len = torch.max(lengths).item()
    ids = torch.arange(0, max_len, out=torch.cuda.LongTensor(max_len))
    mask = (ids < lengths.unsqueeze(1)).byte()
    return mask


class LinearNorm(nn.Module):
    def __init__(self, in_dim, out_dim, bias=True, w_init_gain='linear'):
        super(LinearNorm, self).__init__()
        self.linear_layer = nn.Linear(in_dim, out_dim, bias=bias)

        nn.init.xavier_uniform_(
            self.linear_layer.weight,
            gain=nn.init.calculate_gain(w_init_gain))

    def forward(self, x):
        return self.linear_layer(x)


class ConvNorm(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=1, stride=1,
                 padding=None, dilation=1, bias=True, w_init_gain='linear'):
        super(ConvNorm, self).__init__()
        if padding is None:
            assert(kernel_size % 2 == 1)
            padding = int(dilation * (kernel_size - 1) / 2)

        self.conv = nn.Conv1d(in_channels, out_channels,
                              kernel_size=kernel_size, stride=stride,
                              padding=padding, dilation=dilation, bias=bias)

        nn.init.xavier_uniform_(
            self.conv.weight, gain=nn.init.calculate_gain(w_init_gain))

    def forward(self, signal):
        conv_signal = self.conv(signal)
        return conv_signal


class LocationLayer(nn.Module):
    def __init__(self, attention_n_filters, attention_kernel_size,
                 attention_dim):
        super(LocationLayer, self).__init__()
        padding = int((attention_kernel_size - 1) / 2)
        self.location_conv = ConvNorm(2, attention_n_filters,
                                      kernel_size=attention_kernel_size,
                                      padding=padding, bias=False, stride=1,
                                      dilation=1)
        self.location_dense = LinearNorm(attention_n_filters, attention_dim,
                                         bias=False, w_init_gain='tanh')

    def forward(self, attention_weights_cat):
        processed_attention = self.location_conv(attention_weights_cat)
        processed_attention = processed_attention.transpose(1, 2)
        processed_attention = self.location_dense(processed_attention)
        return processed_attention


class Attention(nn.Module):
    def __init__(self, attention_rnn_dim, embedding_dim, attention_dim,
                 attention_location_n_filters, attention_location_kernel_size):
        super(Attention, self).__init__()
        self.query_layer = LinearNorm(attention_rnn_dim, attention_dim,
                                      bias=False, w_init_gain='tanh')
        self.memory_layer = LinearNorm(embedding_dim, attention_dim, bias=False,
                                       w_init_gain='tanh')
        self.v = LinearNorm(attention_dim, 1, bias=False)
        self.location_layer = LocationLayer(attention_location_n_filters,
                                            attention_location_kernel_size,
                                            attention_dim)
        self.score_mask_value = -float("inf")

    def get_alignment_energies(self, query, processed_memory,
                               attention_weights_cat):
        """
        PARAMS
        ------
        query: decoder output (batch, n_mel_channels * n_frames_per_step)
        processed_memory: processed encoder outputs (B, T_in, attention_dim)
        attention_weights_cat: cumulative and prev. att weights (B, 2, max_time)

        RETURNS
        -------
        alignment (batch, max_time)
        """

        processed_query = self.query_layer(query.unsqueeze(1))
        processed_attention_weights = self.location_layer(attention_weights_cat)
        energies = self.v(torch.tanh(
            processed_query + processed_attention_weights + processed_memory))

        energies = energies.squeeze(-1)
        return energies

    def forward(self, attention_hidden_state, memory, processed_memory,
                attention_weights_cat, mask):
        """
        PARAMS
        ------
        attention_hidden_state: attention rnn last output
        memory: encoder outputs
        processed_memory: processed encoder outputs
        attention_weights_cat: previous and cummulative attention weights
        mask: binary mask for padded data
        """
        alignment = self.get_alignment_energies(
            attention_hidden_state, processed_memory, attention_weights_cat)

        if mask is not None:
            alignment.data.masked_fill_(mask, self.score_mask_value)

        attention_weights = F.softmax(alignment, dim=1)
        attention_context = torch.bmm(attention_weights.unsqueeze(1), memory)
        attention_context = attention_context.squeeze(1)

        return attention_context, attention_weights


class EncoderRNN(nn.Module):
    def __init__(self, input_size, pos_size, hidden_size):
        super(EncoderRNN, self).__init__()

        self.embedding = nn.Embedding(input_size, hidden_size)
        self.pos_embedding = nn.Embedding(pos_size, hidden_size)
        self.mask_embedding = nn.Embedding(2, hidden_size)
        self.gru = nn.GRU(hidden_size, hidden_size // 2, batch_first=True, bidirectional=True)

    def forward(self, inputs, pos, input_lengths):
        embedded = self.embedding(inputs)

        mask = (15 <= inputs) * (inputs <= 40)
        mask = mask.long()
        embedded_mask = self.mask_embedding(mask)

        pos_embedded = self.pos_embedding(pos)

        embedded = embedded + embedded_mask + pos_embedded

        embedded = nn.utils.rnn.pack_padded_sequence(embedded, input_lengths, batch_first=True)
        self.gru.flatten_parameters()
        outputs, hidden = self.gru(embedded)

        outputs, _ = nn.utils.rnn.pad_packed_sequence(outputs, batch_first=True)

        batch_size = inputs.size(0)
        hidden = hidden.transpose(0, 1).contiguous().view(batch_size, -1).unsqueeze(0)
        return outputs, hidden

    def inference(self, inputs, pos):
        embedded = self.embedding(inputs)

        mask = (15 <= inputs) * (inputs <= 40)
        mask = mask.long()
        embedded_mask = self.mask_embedding(mask)

        pos_embedded = self.pos_embedding(pos)

        embedded = embedded + embedded_mask + pos_embedded

        self.gru.flatten_parameters()
        outputs, hidden = self.gru(embedded)

        batch_size = inputs.size(0)
        hidden = hidden.transpose(0, 1).contiguous().view(batch_size, -1).unsqueeze(0)
        return outputs, hidden


class AttnDecoderRNN(nn.Module):
    def __init__(self, hidden_size, output_size, attention_size, attention_location_n_filters,
                 attention_location_kernel_size, max_decoder_steps, teacher_forcing_ratio, dropout_p=0.1):
        super(AttnDecoderRNN, self).__init__()
        self.max_decoder_steps = max_decoder_steps
        self.teacher_forcing_ratio = teacher_forcing_ratio

        self.embedding = nn.Embedding(output_size, hidden_size)
        self.attn = Attention(hidden_size, hidden_size, attention_size,
                              attention_location_n_filters, attention_location_kernel_size)
        self.attn_combine = LinearNorm(hidden_size * 2, hidden_size, w_init_gain='relu')
        self.dropout = nn.Dropout(dropout_p)
        self.gru = nn.GRU(hidden_size, hidden_size, batch_first=True)
        self.out = LinearNorm(hidden_size, output_size)

        self.embedding.weight = self.out.linear_layer.weight

    def decode(self, decoder_input, hidden, encoder_outputs, processed_encoder_outputs,
               attn_weights, attn_weights_cum, mask):
        embedded = self.embedding(decoder_input)
        embedded = self.dropout(embedded)

        attn_weights_cat = torch.cat((attn_weights.unsqueeze(1), attn_weights_cum.unsqueeze(1)), dim=1)
        attn_applied, attn_weights = self.attn(
            embedded, encoder_outputs, processed_encoder_outputs, attn_weights_cat, mask)
        attn_weights_cum += attn_weights
        output = torch.cat((embedded, attn_applied), 1)
        output = self.attn_combine(output).unsqueeze(1)

        output = F.relu(output)
        output, hidden = self.gru(output, hidden)

        output = self.out(output)
        return output, hidden, attn_weights, attn_weights_cum

    def forward(self, inputs, hidden, input_lengths, outputs):
        batch_size = inputs.size(0)
        max_time = inputs.size(1)

        decoder_input = Variable(outputs.data.new(batch_size).fill_(1))

        processed_encoder_outputs = self.attn.memory_layer(inputs)
        attn_weights = Variable(inputs.data.new(batch_size, max_time).zero_())
        attn_weights_cum = Variable(inputs.data.new(batch_size, max_time).zero_())
        mask = ~get_mask_from_lengths(input_lengths)

        decode_outputs = []
        alignments = []
        while len(decode_outputs) < outputs.size(1):
            output, hidden, attn_weights, attn_weights_cum = self.decode(
                decoder_input, hidden, inputs, processed_encoder_outputs, attn_weights, attn_weights_cum, mask)

            output = output.squeeze(1)

            decode_outputs.append(output)

            alignments.append(attn_weights)
            if random.random() < self.teacher_forcing_ratio:
                decoder_input = outputs[:, len(decode_outputs) - 1]
            else:
                _, decoder_input = output.topk(1, dim=-1)
                decoder_input = decoder_input.squeeze(-1)

        decode_outputs = torch.stack(decode_outputs, dim=1)
        alignments = torch.stack(alignments, dim=1)
        return decode_outputs, alignments

    def inference(self, inputs, hidden, input_lengths=None):
        batch_size = inputs.size(0)
        max_time = inputs.size(1)

        decoder_input = Variable(inputs.data.new(batch_size).long().fill_(1))
        processed_encoder_outputs = self.attn.memory_layer(inputs)
        attn_weights = Variable(inputs.data.new(batch_size, max_time).zero_())
        attn_weights_cum = Variable(inputs.data.new(batch_size, max_time).zero_())
        mask = ~get_mask_from_lengths(input_lengths) if input_lengths is not None else None

        decode_outputs = []
        alignments = []
        eos_chk = 0
        while True:
            output, hidden, attn_weights, attn_weights_cum = self.decode(
                decoder_input, hidden, inputs, processed_encoder_outputs, attn_weights, attn_weights_cum, mask)

            output = output.squeeze(1)

            alignments.append(attn_weights)
            _, decoder_input = output.topk(1, dim=-1)
            decoder_input = decoder_input.squeeze(-1)
            eos_chk += (decoder_input == 2).sum().item()

            decode_outputs.append(decoder_input)
            if eos_chk == batch_size:
                break
            elif len(decode_outputs) == self.max_decoder_steps:
                logging.warning("Warning! Reached max decoder steps")
                break

        decode_outputs = torch.stack(decode_outputs, dim=1)
        alignments = torch.stack(alignments, dim=1)
        return decode_outputs, alignments


class Transliterator(nn.Module):
    def __init__(self, hparams):
        super(Transliterator, self).__init__()
        self.output_size = hparams.output_size

        self.encoder = EncoderRNN(hparams.input_size, hparams.pos_size, hparams.hidden_size)
        self.decoder = AttnDecoderRNN(
            hparams.hidden_size, hparams.output_size, hparams.attn_size,
            hparams.attn_location_n_filters, hparams.attn_location_kernel_size,
            hparams.max_decoder_steps, hparams.teacher_forcing_ratio, hparams.dropout_p,
        )

    def forward(self, inputs, pos, input_lengths, outputs, output_lengths):
        encoder_outputs, encoder_hidden = self.encoder(inputs, pos, input_lengths)

        decoder_outputs = self.decoder(encoder_outputs, encoder_hidden, input_lengths, outputs)

        return decoder_outputs

    def inference(self, inputs, pos, input_lengths=None):
        if input_lengths is None:
            encoder_outputs, encoder_hidden = self.encoder.inference(inputs, pos)
        else:
            encoder_outputs, encoder_hidden = self.encoder(inputs, pos, input_lengths)

        decoder_outputs = self.decoder.inference(encoder_outputs, encoder_hidden, input_lengths)

        return decoder_outputs
