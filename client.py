#!/usr/bin/env python3.5
# -*- coding: UTF-8 -*-

import argparse
import grpc

from g2p_pb2_grpc import G2PStub
from g2p_pb2 import Grapheme


class G2PClient(object):
	def __init__(self, remote='127.0.0.1:19001'):
		channel = grpc.insecure_channel(remote)
		self.stub = G2PStub(channel)

	def transliterate(self, sentence_list):
		return self.stub.Transliterate(Grapheme(sentences=sentence_list))


if __name__ == '__main__':
	parser = argparse.ArgumentParser(
		description='g2p client')
	parser.add_argument('-r', '--remote',
						nargs='?',
						dest='remote',
						help='grpc: ip:port',
						type=str,
						default='127.0.0.1:19001') #114.108.173.101:31000
						# default = '114.108.173.101:31000')
	parser.add_argument('-i', '--input',
						nargs='+',
						dest='input',
						help='input english',
						type=str,
						# default=["a b c d e f g h i j k l m n o p q r s t u v w x y and z."])
						# default=["Jackson's case was covered in the live debate in July at Fairfax county where we live."])
						# default=["The advocate showed up in no time."])
						# default=["The advocates finally showed up."])
						# default=["Charlie started to advocate the feminist movement."])
						# default=["The advocates of peace demonstrated quietly."])
						# default=["The allied forces began to ally the war ships perfectly."])
						default=["UK's furlough scheme, which has supported the wages of millions of workers during the Covid pandemic, to be extended until end of December."])
						# default = ["The coordinates show that we are at the right place to coordinate the party."])
						# default=["Even the delegate of Russia had to wear a face mask."])
						# default=["It must be tough to delegate an entire country."])
						# default=["The watch was a fake duplicate of the real one."])
						# default=["It is illegal to duplicate fake watches."])
						# default=["The estimate time of arrival was off by several hours."])
						# default=["The estimated time of arrival was off by several hours."])
						# default=["It is natural to see a moderate use of liquid soap."])
						# default=["Please moderate the exams very strictly."])
						# default=["I can't believe it's already time to present and give out the presents."])
						# default=["The rebels Stuart and Tim will rebel against anyone."])
						# default=["My subordinate kept his head down while I was getting drilled at."])
						# default=["Hello, everyone at Minds lab."])
						# default=["Today, I am going to read my presentation aloud."])
						# default=["I am sure that none has read it."])
						# default=["I am the Text To G to P to Speech model that lives in the server."])
						# default=["Children may abuse prescription medication so keep them out of reach."])
						# default=["Alcohol abuse can be a gateway to other drug abuse."])
						# default=["Please don't desert me on this desert island."])
						# default=["Let's lead the people who drank from the lead poisoned well away."])
						# default=["I'd rather stay and live on this couch and watch the live broadcast of the interview."])
						# default=["Nobody lives in that haunted house, and those who enter are risking their lives."])
						# default=["I know class ends in a minute but please pay attention to minute details."])
						# default=["I am planning to read Harry Potter since most people have read it."])
						# default=["The track record was set a little under a minute. You did record it right?"])
						# default=["Please separate the fruits by color with me and then we can go our separate ways."])
						# default=["Every one of her tear drops tears my heart apart."])
						# default=["I can see tear drops forming in your eyes as Johnny tears up this document."])
						# default=["I have to wind up this flag and pack it before the strong wind blows it away."])
						# default=["The strong winds would have blown me away."])
						# default=["The snake winds in and out of the desert dunes."])
						# default=["I wound up in this ditch and the stitches on my wound almost opened up."])
						# default=["Refrain from getting too close to me and please close the door on your way out."])
						# best_out_checkpoint_5_20210205a 99.62845%

	args = parser.parse_args()

	client = G2PClient(args.remote)

	phoneme = client.transliterate(args.input)
	print(phoneme.sentences)
