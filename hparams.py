import tensorflow as tf

from data import eng_symbols, arpabet_symbols, pos_symbols


def create_hparams(hparams_string=None, verbose=False):
    hparams = tf.contrib.training.HParams(
        ################################
        # Experiment Parameters        #
        ################################
        epochs=2500,
        seed=1234,
        cudnn_enabled=True,
        cudnn_benchmark=False,
        ################################
        # Data Parameters             #
        ################################
        training_data='dataset_train.txt',
        validation_data='dataset_val.txt',
        ################################
        # Model Parameters             #
        ################################
        input_size=len(eng_symbols),
        pos_size=len(pos_symbols),
        hidden_size=512,
        attn_size=128,
        attn_location_n_filters=32,
        attn_location_kernel_size=5,
        max_decoder_steps=1000,
        output_size=len(arpabet_symbols),
        dropout_p=0.1,
        teacher_forcing_ratio=0.5,
        ################################
        # Optimization Hyperparameters #
        ################################
        use_saved_learning_rate=False,
        learning_rate=1e-3,
        weight_decay=1e-6,
        grad_clip_thresh=1.0,
        batch_size=32
    )

    if hparams_string:
        tf.logging.info('Parsing command line hparams: %s', hparams_string)
        hparams.parse(hparams_string)

    if verbose:
        tf.logging.info('Final parsed hparams: %s', hparams.values())

    return hparams
