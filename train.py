#!/usr/bin/env python3.5
# -*- coding: UTF-8 -*-

import argparse
import os
import time
import torch

from torch.utils.data import DataLoader
from shutil import copyfile
from prefetch_generator import BackgroundGenerator

from data import TransliteratorDataset, TextCollate, sequence_to_eng, sequence_to_arpabet, calculate_ler
from logger import TransliteratorLogger
from model import Transliterator
from hparams import create_hparams


class Transliterator2Loss(torch.nn.Module):
    def __init__(self, output_size):
        super(Transliterator2Loss, self).__init__()
        self.output_size = output_size

    def forward(self, model_output, target):
        target.requires_grad = False
        target = target.view(-1)

        pred, _ = model_output
        pred = pred.view(-1, self.output_size)

        loss = torch.nn.CrossEntropyLoss()(pred, target)
        return loss


def to_gpu(x):
    x = x.contiguous().cuda(async=True)
    return torch.autograd.Variable(x)


def parse_batch_train(batch):
    input_padded, pos_padded, input_lengths, output_padded, output_lengths = batch
    input_padded = to_gpu(input_padded).long()
    pos_padded = to_gpu(pos_padded).long()
    input_lengths = to_gpu(input_lengths).long()
    output_padded = to_gpu(output_padded).long()
    output_lengths = to_gpu(output_lengths).long()
    return (input_padded, pos_padded, input_lengths, output_padded, output_lengths), output_padded


def parse_batch_val(batch):
    input_padded, pos_padded, input_lengths, output_padded, _ = batch
    input_padded = to_gpu(input_padded).long()
    pos_padded = to_gpu(pos_padded).long()
    input_lengths = to_gpu(input_lengths).long()
    output_padded = to_gpu(output_padded).long()
    return (input_padded, pos_padded, input_lengths), output_padded


def load_model(hparams):
    model = Transliterator(hparams).cuda()
    return model


def prepare_directories_and_logger(log_directory):
    logger = TransliteratorLogger(log_directory)
    return logger


def prepare_dataloaders(hparams):
    # Get data, data loaders and collate function ready
    trainset = TransliteratorDataset(hparams.training_data, hparams, True)
    valset = TransliteratorDataset(hparams.validation_data, hparams, False)
    collate_fn = TextCollate()

    train_loader = DataLoader(trainset, num_workers=1, shuffle=True,
                              sampler=None,
                              batch_size=hparams.batch_size, pin_memory=True,
                              drop_last=True, collate_fn=collate_fn)
    return train_loader, valset, collate_fn


def warm_start_model(checkpoint_path, model):
    assert os.path.isfile(checkpoint_path)
    print("Warm starting model from checkpoint '{}'".format(checkpoint_path))
    checkpoint_dict = torch.load(checkpoint_path, map_location='cpu')
    model.load_state_dict(checkpoint_dict['state_dict'])
    return model


def load_checkpoint(checkpoint_path, model, optimizer):
    assert os.path.isfile(checkpoint_path)
    print("Loading checkpoint '{}'".format(checkpoint_path))
    checkpoint_dict = torch.load(checkpoint_path, map_location='cpu')
    model.load_state_dict(checkpoint_dict['state_dict'])
    optimizer.load_state_dict(checkpoint_dict['optimizer'])
    learning_rate = checkpoint_dict['learning_rate']
    iteration = checkpoint_dict['iteration']
    print("Loaded checkpoint '{}' from iteration {}" .format(
        checkpoint_path, iteration))
    return model, optimizer, learning_rate, iteration


def validate(model, valset, validation_path, batch_size, collate_fn):
    """Handles all the validation scoring and printing"""
    model.eval()
    with torch.no_grad():
        with open(validation_path, 'w', encoding='utf-8') as wf:
            wf.write("input\ttarget\tprediction\n")
            val_loader = DataLoader(valset, sampler=None, num_workers=1,
                                    shuffle=False, batch_size=batch_size,
                                    pin_memory=True, collate_fn=collate_fn)

            letter_err_cnt = 0
            letter_tot_cnt = 0
            for batch in BackgroundGenerator(val_loader):
                x, y = parse_batch_val(batch)
                prediction, _ = model.inference(*x)

                for batch_idx in range(y.size(0)):
                    input_text = sequence_to_eng(x[0][batch_idx])
                    target_text = sequence_to_arpabet(y[batch_idx])
                    prediction_text = sequence_to_arpabet(prediction[batch_idx])

                    wf.write("{}\t{}\t{}\n".format(input_text, target_text, prediction_text))

                    letter_err_cnt_i, letter_tot_cnt_i = calculate_ler(prediction_text, target_text)
                    letter_err_cnt += letter_err_cnt_i
                    letter_tot_cnt += letter_tot_cnt_i
            ler = letter_err_cnt / letter_tot_cnt
    model.train()
    return ler


def save_checkpoint(model, optimizer, learning_rate, iteration, filepath, hparams):
    print("Saving model and optimizer state at iteration {} to {}".format(
        iteration, filepath))
    torch.save({'iteration': iteration,
                'state_dict': model.state_dict(),
                'optimizer': optimizer.state_dict(),
                'hparams': hparams,
                'learning_rate': learning_rate}, filepath)


def train(output_directory, best_output_directory, log_directory, checkpoint_path, warm_start, hparams):
    for directory in [output_directory, best_output_directory, log_directory]:
        os.makedirs(directory, exist_ok=True)
    validation_path = os.path.join(output_directory, 'validation_last.txt')

    torch.manual_seed(hparams.seed)
    torch.cuda.manual_seed(hparams.seed)

    model = load_model(hparams)
    learning_rate = hparams.learning_rate
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate,
                                 weight_decay=hparams.weight_decay)

    criterion = Transliterator2Loss(hparams.output_size)

    logger = prepare_directories_and_logger(log_directory)

    train_loader, valset, collate_fn = prepare_dataloaders(hparams)

    # Load checkpoint if one exists
    iteration = 0
    epoch_offset = 0
    if checkpoint_path is not None:
        if warm_start:
            model = warm_start_model(checkpoint_path, model)
        else:
            model, optimizer, _learning_rate, iteration = load_checkpoint(
                checkpoint_path, model, optimizer)
            if hparams.use_saved_learning_rate:
                learning_rate = _learning_rate
            for group in optimizer.param_groups:
                group['initial_lr'] = learning_rate
            iteration += 1  # next iteration is iteration + 1
            epoch_offset = max(0, int(iteration / len(train_loader)))

    model.train()
    best_ler = 1.0
    for epoch in range(epoch_offset, hparams.epochs):
        print("Epoch: {}".format(epoch))
        for batch in BackgroundGenerator(train_loader):
            start = time.perf_counter()

            model.zero_grad()
            x, y = parse_batch_train(batch)
            y_pred = model(*x)
            loss = criterion(y_pred, y)
            reduced_loss = loss.item()
            loss.backward()

            grad_norm = torch.nn.utils.clip_grad_norm_(
                model.parameters(), hparams.grad_clip_thresh)

            optimizer.step()

            duration = time.perf_counter() - start
            print("Train loss {}: {} Grad Norm {:.6f} {:.2f}s/it".format(
                iteration, reduced_loss, grad_norm, duration))

            logger.log_training(
                reduced_loss, grad_norm, learning_rate, duration, iteration)

            iteration += 1

        ler = validate(model, valset, validation_path, hparams.batch_size, collate_fn)

        print("Validation LER: {}  ".format(ler))
        logger.log_validation(ler, model, y_pred, epoch)
        checkpoint_last_path = os.path.join(output_directory, "checkpoint_last")
        save_checkpoint(model, optimizer, learning_rate, epoch, checkpoint_last_path, hparams)

        if ler < best_ler:
            best_ler = ler
            checkpoint_best_path = os.path.join(best_output_directory, "checkpoint_{}".format(epoch))
            validation_best_path = os.path.join(best_output_directory, 'validation_{}.txt'.format(epoch))
            copyfile(checkpoint_last_path, checkpoint_best_path)
            copyfile(validation_path, validation_best_path)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output_directory', type=str, default='out',
                        help='directory to save last checkpoints')
    parser.add_argument('-b', '--best_output_directory', type=str, default='best_out',
                        help='directory to save best checkpoint and validation files')
    parser.add_argument('-l', '--log_directory', type=str, default='out/tensorboard',
                        help='directory to save tensorboard logs')
    parser.add_argument('-c', '--checkpoint_path', type=str, default=None,
                        required=False, help='checkpoint path')
    parser.add_argument('--warm_start', action='store_true',
                        help='load the model only (warm start)')
    parser.add_argument('--hparams', type=str,
                        required=False, help='comma separated name=value pairs')

    args = parser.parse_args()
    hparams = create_hparams(args.hparams)

    torch.backends.cudnn.enabled = hparams.cudnn_enabled
    torch.backends.cudnn.benchmark = hparams.cudnn_benchmark

    print("cuDNN Enabled:", hparams.cudnn_enabled)
    print("cuDNN Benchmark:", hparams.cudnn_benchmark)

    train(args.output_directory, args.best_output_directory, args.log_directory, args.checkpoint_path, args.warm_start, hparams)
