import random

from tensorboardX import SummaryWriter
from plotting_utils import plot_alignment_to_numpy


class TransliteratorLogger(SummaryWriter):
    def __init__(self, logdir):
        super(TransliteratorLogger, self).__init__(logdir)

    def log_training(self, reduced_loss, grad_norm, learning_rate, duration,
                     iteration):
        self.add_scalar("training.loss", reduced_loss, iteration)
        self.add_scalar("grad.norm", grad_norm, iteration)
        self.add_scalar("learning.rate", learning_rate, iteration)
        self.add_scalar("duration", duration, iteration)

    def log_validation(self, reduced_loss, model, y_pred, iteration):
        self.add_scalar("validation.ler", reduced_loss, iteration)
        # plot distribution of parameters
        for tag, value in model.named_parameters():
            tag = tag.replace('.', '/')
            try:
                self.add_histogram(tag, value.data.cpu().numpy(), iteration)
            except Exception as e:
                print(tag, value.data.cpu().numpy())
                raise e

        _, alignments = y_pred
        # plot alignment, mel target and predicted, gate target and predicted
        idx = random.randint(0, alignments.size(0) - 1)
        self.add_image(
            "alignment",
            plot_alignment_to_numpy(alignments[idx].data.cpu().numpy().T),
            iteration)
