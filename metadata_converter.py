import argparse
from functools import partial
from tqdm import tqdm
from contextlib import closing
from multiprocessing import Pool

from client import G2PClient


def parallel_run(fn, items, desc="", n_cpu=1):
    results = []

    if n_cpu > 1:
        with closing(Pool(n_cpu)) as pool:
            for out in tqdm(pool.imap_unordered(
                    fn, items), total=len(items), desc=desc):
                if out is not None:
                    results.append(out)
    else:
        for item in tqdm(items, total=len(items), desc=desc):
            out = fn(item)
            if out is not None:
                results.append(out)

    return results


def g2p_func(text, remote):
    g2p_client = G2PClient(remote)
    pred = g2p_client.transliterate([text])
    pred = pred.sentences[0]
    return pred


def test_batch(data_list, n_cpu, **kargv):
    test_fn = partial(g2p_func, **kargv)

    results = parallel_run(test_fn, data_list,
                           desc="Applying g2p to metadata", n_cpu=n_cpu)
    return results


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--remote',
                        nargs='?',
                        help='g2p remote grpc server=ip:port',
                        type=str,
                        default='127.0.0.1:19001')
    parser.add_argument('-i', '--input',
                        nargs='?',
                        help='input file',
                        type=str,
                        required=True)
    parser.add_argument('-o', '--output',
                        nargs='?',
                        help='output file',
                        type=str,
                        default='output.txt')
    parser.add_argument('-n', '--n_cpu',
                        type=int,
                        default=1,
                        help='number of processes')

    args = parser.parse_args()

    data_list = []
    spare_list = []
    with open(args.input, 'r', encoding='utf-8') as rf:
        for line in rf.readlines():
            dirs, text, speaker = line.strip().split('|')
            data_list.append(text)
            spare_list.append([dirs, speaker])

    # main loop: apply g2p to text of each line
    results = test_batch(data_list, args.n_cpu, remote=args.remote)

    assert len(results) == len(spare_list), \
        "number of results do not match input; thus can't write"

    with open(args.output, 'w', encoding='utf-8') as wf:
        for data, spare in zip(results, spare_list):
            dirs = spare[0]
            text = data
            speaker = spare[1]
            wf.write('%s\n' % "|".join([dirs, text, speaker]))
