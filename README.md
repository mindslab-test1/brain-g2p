# brain-g2p

TTS에 사용할 목적으로 자소(grapheme)를 음소(phoneme)로 변환해주는 딥러닝 기반 엔진이다.
여기에서의 자소는 Alphabet, 음소는 강세가 없는 Arpabet이다.
모델은 Tacotron2에서 사용된 Location-Sensitive Attention을 기반으로 한 seq2seq 구조이다.

자세한 내용 및 사용법은 https://pms.maum.ai/confluence/x/ppcjAg 참조.

주의: 아래 내용은 다소 outdated 되어있어 별도 확인이 필요함.

## Setup
1. `pip install -r requirements.txt`
1. `python -m grpc.tools.protoc --proto_path=. --python_out=. --grpc_python_out=. g2p.proto`
1. `python -m spacy download en_core_web_sm`

## Setup - Windows
1. `pip install -r requirements_win.txt`

## Levenshtein Install Issue (Troubleshoot)

### INSTALL Latest Visual Studio (Community):

https://visualstudio.microsoft.com/downloads/

#### FROM:

https://www.lfd.uci.edu/~gohlke/pythonlibs/#python-levenshtein

#### DOWNLOAD:

1. `python_Levenshtein-0.12.0-cp35-cp35m-win_amd64.whl`

#### INSTALL:

1. `pip install python_Levenshtein-0.12.0-cp35-cp35m-win_amd64.whl`

## Train
1. `python train.py`

## Run Server
1. `python server.py -m model.pt`

## Run Client
1. `python client.py`

## TODO
- [ ] dockerfile