FROM nvcr.io/nvidia/pytorch:19.06-py3

RUN python3 -m pip --no-cache-dir install --upgrade \
        tensorflow==1.9.0 \
        tensorboardX==1.2 \
        grpcio==1.38.0 \
        grpcio-tools==1.38.0 \
        grpcio-health-checking==1.38.0 \
        protobuf==3.15.8 \
        prefetch_generator==1.0.1 \
        spacy \
        python-Levenshtein==0.12.0 && \
    python3 -m spacy download en_core_web_sm && \
# ==================================================================
# config & cleanup
# ------------------------------------------------------------------
    ldconfig && \
    apt-get clean && \
    apt-get autoremove && \
    rm -rf /var/lib/apt/lists/* /tmp/* /workspace/*

RUN mkdir /root/g2p

COPY . /root/g2p

RUN cd /root/g2p &&\
    python3 -m grpc.tools.protoc --proto_path=. --python_out=. --grpc_python_out=. g2p.proto
